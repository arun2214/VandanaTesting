package LocalTest.RunLocalTest;
//import static org.testng.Assert.assertTrue;
import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.asserts.Assertion;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.testng.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import com.relevantcodes.extentreports.LogStatus;
@Listeners(LocalTest.RunLocalTest.Listners.class)

public class Login extends PageObjects {
	
	public static void test1_Login(WebDriver driver) throws Exception  {
		
	//try {	
		Data_Utility.TestDataInteraction(Constant.Path_TestData + Constant.File_TestData,"Login","TestCase1");
		String sUserName = Data_Utility.GetObjDict().get("UserName"); 
		String sPassword = Data_Utility.GetObjDict().get("Pass"); 
		String sChannel  = Data_Utility.GetObjDict().get("Transaction");
		double sConfig_UserName = Double.valueOf(Data_Utility.GetObjDict().get("Config_UserName"));
		
		if (Math.round(sConfig_UserName)==1) {
			Listners.test.info("started G1...");
			driver.findElement(userName1).sendKeys(sUserName);
			Listners.test.info("Login.."+sUserName);
		} 
		    Thread.sleep(4000);
		    driver.findElement(Next1).click();
		    Thread.sleep(4000);
		//To locate input field for password and enter value
		driver.findElement(passWord).sendKeys(sPassword);
		//To locate and click on login button
		driver.findElement(cLickLogin).click();	
        Thread.sleep(4000);
		System.out.println("Test thread id is for Test1 :"+Thread.currentThread().getId());
		System.out.println("pass");
	//}
	   //catch (Exception e) {
	 //  System.out.println("Something went wrong.");
	//} 
		
	}
	
	public static void test2_Login(WebDriver driver) throws Exception  {
		//try {
		Data_Utility.TestDataInteraction(Constant.Path_TestData + Constant.File_TestData,"Login","TestCase2");
		String sUserName = Data_Utility.GetObjDict().get("UserName"); 
		String sPassword = Data_Utility.GetObjDict().get("Pass"); 
		String sChannel  = Data_Utility.GetObjDict().get("Transaction");
		Listners.test.info("started G2...");
		driver.findElement(userName1).sendKeys(sUserName);
		//To locate input field for password and enter value
		Listners.test.info("Login.."+sUserName);
		Thread.sleep(4000);
	    driver.findElement(Next1).click();
	    Thread.sleep(4000);
		driver.findElement(passWord).sendKeys(sPassword);
		//To locate and click on login button
		driver.findElement(cLickLogin).click();
		Thread.sleep(4000);
		System.out.println("Test thread id is for Test2 :"+Thread.currentThread().getId());
		System.out.println("pass");//}
	 
	//catch (Exception e) {
		  // System.out.println("Something went wrong.");
		//}
	}

	public static void test3_Login(WebDriver driver) throws Exception  {
		Data_Utility.TestDataInteraction(Constant.Path_TestData + Constant.File_TestData,"Login","TestCase3");
 		String sUserName = Data_Utility.GetObjDict().get("UserName"); 
		String sPassword = Data_Utility.GetObjDict().get("Pass"); 
		String sChannel  = Data_Utility.GetObjDict().get("Transaction");
		driver.findElement(userName1).sendKeys(sUserName);
		Thread.sleep(4000);
		 driver.findElement(Next1).click();
		//To locate input field for password and enter value
		driver.findElement(passWord).sendKeys(sPassword);
		//To locate and click on login button
		driver.findElement(cLickLogin).click();
        Thread.sleep(4000);
		System.out.println("Test thread id is for Test3 :"+Thread.currentThread().getId());
		System.out.println("pass");
	}
	public static void test4_Login(WebDriver driver) throws Exception  {
		Data_Utility.TestDataInteraction(Constant.Path_TestData + Constant.File_TestData,"Login","TestCase4");
 		String sUserName = Data_Utility.GetObjDict().get("UserName"); 
		String sPassword = Data_Utility.GetObjDict().get("Pass"); 
		String sChannel  = Data_Utility.GetObjDict().get("Transaction");
		driver.findElement(userName1).sendKeys(sUserName);
		//To locate input field for password and enter value
		driver.findElement(passWord).sendKeys(sPassword);
		//To locate and click on login button
		driver.findElement(cLickLogin).click();
        Thread.sleep(4000);
		System.out.println("Test thread id is for Test4 :"+Thread.currentThread().getId());
		System.out.println("pass");
	}

}
